package erikar.org.ehrm3;

import android.app.Service;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.getpebble.android.kit.PebbleKit;
import com.getpebble.android.kit.util.PebbleDictionary;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

public class hrmService extends Service {

    private static final UUID WATCHAPP_UUID = UUID.fromString("092a05f8-1885-4ea6-9687-fd6fde233bae");

    public static String HEART_RATE_MEASUREMENT = "00002a37-0000-1000-8000-00805f9b34fb";
    public static String BATTERY_MEASUREMENT = "00002a19-0000-1000-8000-00805f9b34fb";
    public static final String EXTRAS_DEVICE_NAME = "DEVICE_NAME";
    public static final String EXTRAS_DEVICE_ADDRESS = "DEVICE_ADDRESS";
    public final static String TIME_DATA =
            "com.example.bluetooth.le.TIME_DATA";

    private long startTime;
    private long lastTime = 0;

    private String lastHR = "0";

    private HRM_Thread t;
    private boolean running = true;

    private String mDeviceName;
    private String mDeviceAddress;

    private ArrayList<ArrayList<BluetoothGattCharacteristic>> mGattCharacteristics =
            new ArrayList<ArrayList<BluetoothGattCharacteristic>>();
    private boolean mConnected = false;
    private BluetoothGattCharacteristic mNotifyCharacteristic;

    private BluetoothLeService mBluetoothLeService;

    private Intent gattServiceIntent;

    public String TAG = "eHRM";
    private PebbleKit.PebbleDataReceiver appMessageReciever;

    private static final int
            KEY_BUTTON = 0,
            KEY_VIBRATE = 1,
            BUTTON_UP = 0,
            BUTTON_SELECT = 1,
            BUTTON_DOWN = 2;

    private Handler handler = new Handler();


    public hrmService() {
    }

    // Code to manage Service lifecycle.
    private final ServiceConnection mServiceConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName componentName, IBinder service) {
            mBluetoothLeService = ((BluetoothLeService.LocalBinder) service).getService();
            if (!mBluetoothLeService.initialize()) {
                Log.e(TAG, "Unable to initialize Bluetooth");
                //finish();
            }
            // Automatically connects to the device upon successful start-up initialization.
            mBluetoothLeService.connect(mDeviceAddress);
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            mBluetoothLeService = null;
        }
    };

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        //throw new UnsupportedOperationException("Not yet implemented");
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        Toast.makeText(this, "The new Service was Created", Toast.LENGTH_LONG).show();
        //PebbleKit.startAppOnPebble(getApplicationContext(), WATCHAPP_UUID);
        PebbleKit.startAppOnPebble(getApplicationContext(), WATCHAPP_UUID);

        if(appMessageReciever == null) {
            appMessageReciever = new PebbleKit.PebbleDataReceiver(WATCHAPP_UUID) {

                @Override
                public void receiveData(Context context, int transactionId, PebbleDictionary data) {
                    // Always ACK
                    PebbleKit.sendAckToPebble(context, transactionId);

                    // What message was received?
                    if(data.getInteger(KEY_BUTTON) != null) {
                        // KEY_BUTTON was received, determine which button
                        final int button = data.getInteger(KEY_BUTTON).intValue();

                        switch(button) {
                            case 1:
                                int i = 0;
                                break;
                        }

                    }
                }
            };

            // Add AppMessage capabilities
            PebbleKit.registerReceivedDataHandler(this, appMessageReciever);
        }

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        // Let it continue running until it is stopped.
        mDeviceAddress = intent.getStringExtra(EXTRAS_DEVICE_ADDRESS);
        mDeviceName = intent.getStringExtra(EXTRAS_DEVICE_NAME);

        gattServiceIntent = new Intent(this, BluetoothLeService.class);
        
        Thread t = new Thread(){
            public void run(){

                bindService(gattServiceIntent, mServiceConnection, BIND_AUTO_CREATE);
            }
        };
        t.start();

        try {
            Thread.sleep(1000);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        registerReceiver(mGattUpdateReceiver, makeGattUpdateIntentFilter());

        if (mBluetoothLeService != null) {
            final boolean result = mBluetoothLeService.connect(mDeviceAddress);
            Log.d(TAG, "Connect request result=" + result);
        }

        Toast.makeText(this, "Service Started", Toast.LENGTH_LONG).show();



        t = new HRM_Thread();
        t.start();
        return START_STICKY;
    }
    private final void trackCharacteristic(BluetoothGattCharacteristic btChar) {
        final BluetoothGattCharacteristic characteristic = btChar;
        final int charaProp = characteristic.getProperties();
        if ((charaProp | BluetoothGattCharacteristic.PROPERTY_READ) > 0) {
            // If there is an active notification on a characteristic, clear
            // it first so it doesn't update the data field on the user interface.
            if (mNotifyCharacteristic != null) {
                mBluetoothLeService.setCharacteristicNotification(
                        mNotifyCharacteristic, false);
                mNotifyCharacteristic = null;
            }
            mBluetoothLeService.readCharacteristic(characteristic);
        }
        if ((charaProp | BluetoothGattCharacteristic.PROPERTY_NOTIFY) > 0) {
            mNotifyCharacteristic = characteristic;
            mBluetoothLeService.setCharacteristicNotification(
                    characteristic, true);
        }
        return;
    }

    @Override
    public void onDestroy() {
        Toast.makeText(this, "Service Destroyed", Toast.LENGTH_LONG).show();
        running = false;
        unregisterReceiver(mGattUpdateReceiver);
        unbindService(mServiceConnection);
        mBluetoothLeService = null;
        PebbleKit.closeAppOnPebble(getApplicationContext(), WATCHAPP_UUID);

        if(appMessageReciever != null) {
            unregisterReceiver(appMessageReciever);
            appMessageReciever = null;
        }
    }

    // Demonstrates how to iterate through the supported GATT Services/Characteristics.
    // In this sample, we populate the data structure that is bound to the ExpandableListView
    // on the UI.
    //service = 0000180d-0000-1000-8000-00805f9b34fb
    //charasteristic = 00002a37-0000-1000-8000-00805f9b34fb
    private void displayGattServices(List<BluetoothGattService> gattServices) {
        if (gattServices == null) return;
        String uuid = null;
        String unknownServiceString = getResources().getString(R.string.unknown_service);
        String unknownCharaString = getResources().getString(R.string.unknown_characteristic);
        ArrayList<HashMap<String, String>> gattServiceData = new ArrayList<HashMap<String, String>>();
        ArrayList<ArrayList<HashMap<String, String>>> gattCharacteristicData
                = new ArrayList<ArrayList<HashMap<String, String>>>();
        mGattCharacteristics = new ArrayList<ArrayList<BluetoothGattCharacteristic>>();

        // Loops through available GATT Services.
        for (BluetoothGattService gattService : gattServices) {
            HashMap<String, String> currentServiceData = new HashMap<String, String>();
            uuid = gattService.getUuid().toString();
//            currentServiceData.put(
//                    LIST_NAME, SampleGattAttributes.lookup(uuid, unknownServiceString));
//            currentServiceData.put(LIST_UUID, uuid);
            gattServiceData.add(currentServiceData);

            ArrayList<HashMap<String, String>> gattCharacteristicGroupData =
                    new ArrayList<HashMap<String, String>>();
            List<BluetoothGattCharacteristic> gattCharacteristics =
                    gattService.getCharacteristics();
            ArrayList<BluetoothGattCharacteristic> charas =
                    new ArrayList<BluetoothGattCharacteristic>();

            // Loops through available Characteristics.
            for (BluetoothGattCharacteristic gattCharacteristic : gattCharacteristics) {
                charas.add(gattCharacteristic);
                HashMap<String, String> currentCharaData = new HashMap<String, String>();
                uuid = gattCharacteristic.getUuid().toString();

                //"00002a37-0000-1000-8000-00805f9b34fb";
                if (uuid.equals(HEART_RATE_MEASUREMENT)) {
                    trackCharacteristic(gattCharacteristic);
                }

//                currentCharaData.put(
//                        LIST_NAME, SampleGattAttributes.lookup(uuid, unknownCharaString));
//                currentCharaData.put(LIST_UUID, uuid);
                gattCharacteristicGroupData.add(currentCharaData);
            }
            mGattCharacteristics.add(charas);
            gattCharacteristicData.add(gattCharacteristicGroupData);
        }

    }

    private class HRM_Thread extends Thread{
        @Override
        public void run() {
            super.run();
            String time = null;
            while (running) {
                //Log.v("erik", "hello");
                try {
                    sleep(1000);

                    long dt = System.currentTimeMillis() - startTime;
                    if (TimeUnit.MILLISECONDS.toHours(dt) < 10) {
                        time = String.format("%1d:%02d:%02d",
                                TimeUnit.MILLISECONDS.toHours(dt),
                                TimeUnit.MILLISECONDS.toMinutes(dt) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(dt)),
                                TimeUnit.MILLISECONDS.toSeconds(dt) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(dt))
                        );
                    }
                    else {
                        time = String.format("%02d:%02d",
                                TimeUnit.MILLISECONDS.toHours(dt),
                                TimeUnit.MILLISECONDS.toMinutes(dt) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(dt))
                        );
                    }

                    PebbleDictionary out = new PebbleDictionary();
                    out.addString(1, lastHR);
                    out.addString(2, time);
                    PebbleKit.sendDataToPebble(getApplicationContext(), WATCHAPP_UUID, out);

                    broadcastUpdate(TIME_DATA, time);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }
    private final BroadcastReceiver mGattUpdateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();
            //Log.v(TAG, "battery =  " + action);
            if (BluetoothLeService.ACTION_GATT_CONNECTED.equals(action)) {
                mConnected = true;
                startTime = System.currentTimeMillis();
            } else if (BluetoothLeService.ACTION_GATT_DISCONNECTED.equals(action)) {
                mConnected = false;
            } else if (BluetoothLeService.ACTION_GATT_SERVICES_DISCOVERED.equals(action)) {
                // Show all the supported services and characteristics on the user interface.
                displayGattServices(mBluetoothLeService.getSupportedGattServices());
            } else if  (BluetoothLeService.ACTION_BATTERY_DATA_AVAILABLE.equals(action)) {
                displayBatteryData(intent.getIntExtra(BluetoothLeService.EXTRA_BATTERY_DATA, 0));
            } else if (BluetoothLeService.ACTION_DATA_AVAILABLE.equals(action)) {
                displayData(intent.getStringExtra(BluetoothLeService.EXTRA_DATA));
            }
        }
    };

    private void displayBatteryData(int data) {
//        ProgressBar p = (ProgressBar) findViewById(R.id.progressBar);
//        p.setProgress(data);
        String s = String.format("Battery: %d", data);
        Log.i(TAG, s);
    }

    private void displayData(String data) {
        if (data != null) {

//            mDataField.setText(data);
            //send broadcast to acLog.i(TAG, data);tivity with data
            lastHR = data;
//            Log.i(TAG, data);
//            PebbleDictionary out = new PebbleDictionary();
//            out.addString(1, data);
//            PebbleKit.sendDataToPebble(getApplicationContext(), WATCHAPP_UUID, out);


        }
    }

    private void broadcastUpdate(final String action,
                                 final String time) {
        final Intent intent = new Intent(action);
        intent.putExtra(TIME_DATA, time);
        sendBroadcast(intent);
    }

    private static IntentFilter makeGattUpdateIntentFilter() {
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(BluetoothLeService.ACTION_GATT_CONNECTED);
        intentFilter.addAction(BluetoothLeService.ACTION_GATT_DISCONNECTED);
        intentFilter.addAction(BluetoothLeService.ACTION_GATT_SERVICES_DISCOVERED);
        intentFilter.addAction(BluetoothLeService.ACTION_DATA_AVAILABLE);
        intentFilter.addAction(BluetoothLeService.ACTION_BATTERY_DATA_AVAILABLE);
        return intentFilter;
    }

}
